import scala.sys.process._
import java.io.File

val keyAlias = "youralias"
val storepass = "XXXXXXXXXXXXXXX"
val keypass = "XXXXXXXXXXXXXXX"
val keystorePath = "/path/to/your/.keystore"

val zipAlignPath = "/path/to/zipalign/for/example/android-sdk-linux/build-tools/22.0.1/zipalign"

if (args.length != 1) {
  println("please specify the apk file")

  sys.exit()
}

val firstArg = args(0)

val file = new File(firstArg)

if(!file.exists() || !file.isFile) {
	println("apk file does not exist")

	sys.exit()
}

val apkPath = file.getCanonicalPath

println(apkPath)

val jarsignerCmd = """jarsigner -storepass """ + storepass + """ -keypass """ + keypass + """ -verbose -sigalg MD5withRSA -digestalg SHA1 -keystore """ + keystorePath + " " + apkPath + " " + keyAlias

println(jarsignerCmd)
//jarsignerCmd.!!; we dont want to output the string of this command

jarsignerCmd.!

val alignedPath = apkPath + ".aligned.apk"

val zipalignCmd = zipAlignPath + " -v 4 " + apkPath + " " + alignedPath

println(zipalignCmd)

zipalignCmd.!

val nautilusCmd = "nautilus " + alignedPath

println(nautilusCmd)

nautilusCmd.!

