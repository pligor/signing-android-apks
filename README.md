CURRENTLY IS RECOMMENDED TO SKIP USING THIS TOOL
THE SAME FUNCTIONALITY CAN BE FOUND BY EXECUTING `android:package-release` IN `sbt` WHILE HAVING SET THE FOLLOWING FOUR VALUES IN `project.properties` (`local.properties`):

* `key.alias.password: YOUR-KEY-ALIAS-PASSWORD`
* `key.store: /path/to/your/.keystore`
* `key.store.password: YOUR-KEY-PASSWORD`
* `key.store.password: YOUR-KEY-STORE-PASSWORD`

see here for more information: [https://github.com/pfn/android-sdk-plugin](https://github.com/pfn/android-sdk-plugin)

If still want to use this utility then follow the instructions below:

execute `android:package-release` in `sbt` and then execute in terminal:  
`scala signing.scala path/to/unsigned_and_unaligned.apk`

do **not** forget to set the constants (vals) inside signing.scala, like key alias, key password etc.

This scala script is simply a wrapper for these two commands:

`jarsigner -storepass XXXXXXXXXXXXXXX -keypass XXXXXXXXXXXXXXX -verbose -sigalg MD5withRSA -digestalg SHA1 -keystore ~/path/to/.keystore some_application.apk ALIASHERE`  
*jarsigner is found inside local java folder*

and

`zipalign -v 4 project_name-unaligned.apk project_name.apk`  
*zipalign is found inside android-sdk-linux folder*
